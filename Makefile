IMAGE := manycore-img
CONFIG := 4
RED := 1.2
BLUE := 0.8
GREEN := 0.8

all : png_transform.o
	gcc -o png_transform png_transform.o -L/path/to/dir_containing/libpng -lpng
png_transform.o : png_transform.c
	gcc -o png_transform.o -c png_transform.c -L/path/to/dir_containing/libpng -lpng
	
run :
	./png_transform	imgs/$(IMAGE).png imgs_transformed/$(IMAGE).png $(CONFIG) $(RED) $(BLUE) $(GREEN) 
clean :
	rm png_transform