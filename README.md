# Kalray_test

## environment
### For compilation the code   
make 
### For running the code  
To launch code, run this line in terminal :  
make run IMAGE=image-name CONFIG=config-option  RED=float BLUE= float GREEN= float  
example
make run IMAGE=manycore-img CONFIG=4  RED=0.5 BLUE= 1 GREEN= 0.8  
Note Well : if we launch `make run` without option, is the equivalent to `make run IMAGE=manycore-img CONFIG=4  RED=1.2 BLUE= 0.8 GREEN= 0.8`  

## Debug  
For debugging, I used printfs
1- We haven't been able to convert an image using the program. For some reason, the images are never transformed :  
Due to this :
```
	if (png_get_color_type(img->png_ptr, img->info_ptr) != PNG_COLOR_TYPE_RGBA)
		printf("[process_file] color_type of input file must be PNG_COLOR_TYPE_RGBA (%d) (is %d)", PNG_COLOR_TYPE_RGBA, png_get_color_type(img->png_ptr, img->info_ptr));
		return 1;
```
return 1 is always called so transformation never done  
2- Once, a colleague succeeded in running it but noticed some segfaults, can you investigate ?  
Just a bug on the for parametres : replace `for (y = 0; y < img->w; y++)` by `for (y = 0; y < img->h; y++)` solve the problem. 
the process of debug :  
1- use the function `printf` to see where we have the segfaults before, after or in the loop for --> in the loop for  
2- use the function `printf` on the loop counter to see in which loop for we are blocked --> no problem in the first loop for so the problem is in the second one and it is blocked always in the same value (the loop limit value is wrong).  
3- change the loop limit parameter to the right value which is img->h  

## Improvement
1- Gather the treatment of image in the same embedded loop
before :
```
	for (x = 0; x < img->w; x++)
	  {
	  for (y = 0; y < img->h; y++)
	    {
	      png_byte *row = img->row_pointers[y];
	      png_byte *ptr = &(row[x * 4]);
	      /* set red value to 0 */
	      ptr[0]  = 0;
	    }
	  }

	for (x = 0; x < img->w; x++) {
		for (y = 0; y < img->h; y++) {
			png_byte *row = img->row_pointers[y];
			png_byte *ptr = &(row[x * 4]);
			/* Then set green value to the blue one */
			ptr[1]  = ptr[2];
		}
	}
```
after  
```
	for (x = 0; x < img->w; x++)
	  {
	  for (y = 0; y < img->h; y++)
	    {
	      png_byte *row = img->row_pointers[y];
	      png_byte *ptr = &(row[x * 4]);
	      /* set red value to 0 */
	      ptr[0]  = 0;
		  /* Then set green value to the blue one */
		  ptr[1]  = ptr[2];
	    }
	  }
```
2- Try to change multiplication by shift if possible  
before  
```
png_byte *ptr = &(row[x * 4]);
```
after  
```
png_byte *ptr = &(row[x << 2]);
```

## Add transformation 
* the old transformation is avaible and function if we launch make run CONFIG=0
* The choice between the transformations must be done at start-up using parameters : via CONFIG we can chose wich configuration will be apllied :  
0 : default  
1 : Only red  
2 : Only blue  
3 : Only green  
4 : mixed red, blue and green    

## time spent on work
4 hours 